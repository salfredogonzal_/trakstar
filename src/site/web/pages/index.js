function HomePage() {
    const styles = {
        section: {
            backgroundColor: "#9A616D",
        },
        image: {
            borderRadius: "1rem 0 0 1rem",
        }
    }
    return (
        <div>
            <div className="vh-100">
                <div className="container">
                    <form>
                        <h3>Sign In</h3>
                        <div className="mb-3">
                            <label>Username</label>
                            <input
                                type="text"
                                className="form-control"
                            />
                        </div>
                        <div className="mb-3">
                            <label>Password</label>
                            <input
                                type="password"
                                className="form-control"
                            />
                        </div>
                        <div className="d-grid">
                            <button type="submit" className="btn btn-primary">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HomePage;
