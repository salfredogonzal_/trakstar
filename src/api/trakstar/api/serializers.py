from rest_framework import serializers
from django.contrib.auth.models import User

from kudos.models import Enterprise, Kudo, Profile


class EnterpriseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enterprise
        fields = ("name",)
        read_only_fields = ("name",)


class KudoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kudo
        fields = "__all__"


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = "__all__"
