from django.urls import path
from .views import EnterpriseAPIView, KudosAPIView, ProfileAPIView

urlpatterns = [
    path("enterprise/", EnterpriseAPIView.as_view()),
    path("kudos/", KudosAPIView.as_view()),
    path("profile/", ProfileAPIView.as_view()),
]
