from django.shortcuts import render

from rest_framework import generics
from kudos.models import Enterprise, Kudo, Profile
from .serializers import EnterpriseSerializer, KudoSerializer, ProfileSerializer


class EnterpriseAPIView(generics.ListAPIView):
    queryset = Enterprise.objects.all()
    serializer_class = EnterpriseSerializer


class KudosAPIView(generics.ListAPIView):
    queryset = Kudo.objects.all()
    serializer_class = KudoSerializer


class ProfileAPIView(generics.ListAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
