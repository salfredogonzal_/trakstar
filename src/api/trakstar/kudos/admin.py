from django.contrib import admin

from .models import Enterprise, Kudo, Profile

admin.site.register(Enterprise)
admin.site.register(Kudo)
admin.site.register(Profile)
