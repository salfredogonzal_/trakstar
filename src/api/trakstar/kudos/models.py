from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone


class Enterprise(models.Model):
    """
    The model to preview a company
    """

    name = models.CharField(max_length=100)

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)
    status = models.PositiveSmallIntegerField(blank=True, null=True)

    def __str__(self):
        return self.name


class Profile(models.Model):
    name = models.OneToOneField(User, on_delete=models.CASCADE)
    enterprise = models.ForeignKey(
        Enterprise, on_delete=models.SET_NULL, null=True, blank=True
    )

    def __str__(self):
        return self.name.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Kudo(models.Model):
    """
    The model to show the Kudos
    """

    name = models.CharField(max_length=65)
    message = models.CharField(max_length=255)
    date = models.DateTimeField()

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)
    status = models.PositiveSmallIntegerField(blank=True, null=True)

    def __str__(self):
        return self.name
