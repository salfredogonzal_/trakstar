# Generated by Django 4.0.6 on 2022-07-12 03:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kudos', '0004_profile_enterprise'),
    ]

    operations = [
        migrations.RenameField(
            model_name='profile',
            old_name='user',
            new_name='name',
        ),
    ]
